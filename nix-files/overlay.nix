self: super:

let
  lib = self.pkgs.lib;
  stdenv = self.pkgs.stdenv;
  pythonEnv = super.python3;
in
{
  ns-3 = super.ns-3.override {
    build_profile = "debug";
    modules = [ "all_modules" "core" "network" "internet" "point-to-point" "point-to-point-layout" "fd-net-device" "netanim" "flow-monitor" "mobility" ];
  };

  my-ns-3 = self.ns-3.overrideAttrs
    (oldAttrs: rec {
      src = super.fetchFromGitLab {
          owner = "nsnam";
          repo = "ns-3-dev";
          rev = "master";
          sha256 = "1cm7dimq5iww9p4pdccii2qjc64agg5wmrnl107wlfny0r4rldj7";
      };
      patches = [ ];
      doCheck = false;
    });
}
