/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 SEBASTIEN DERONNE
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Sebastien Deronne <sebastien.deronne@gmail.com>
 */

#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/on-off-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/attribute.h"
#include "ns3/boolean.h"
#include "ns3/callback.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/constant-velocity-helper.h"
#include "ns3/constant-velocity-mobility-model.h"
#include "ns3/double.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/log.h"
#include "ns3/mobility-helper.h"
#include "ns3/mobility-model.h"
#include "ns3/node-list.h"
#include "ns3/node.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/packet.h"
#include "ns3/propagation-delay-model.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/random-variable-stream.h"
#include "ns3/simulator.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/uinteger.h"
#include "ns3/wifi-mac-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"

// This is a simple example in order to show how to configure an IEEE 802.11ac Wi-Fi network.
//
// It outputs the UDP or TCP goodput for every VHT MCS value, which depends on the MCS value (0 to 9, where 9 is
// forbidden when the channel width is 20 MHz), the channel width (20, 40, 80 or 160 MHz) and the guard interval (long
// or short). The PHY bitrate is constant over all the simulation run. The user can also specify the distance between
// the access point and the station: the larger the distance the smaller the goodput.
//
// The simulation assumes a single station in an infrastructure network:
//
//  STA     AP
//    *     *
//    |     |
//   n1     n2
//
//Packets in this simulation belong to BestEffort Access Class (AC_BE).

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("vht-wifi-network");

void move()
{
  Ptr<Node> node = NodeList::GetNode(4);
  Ptr<MobilityModel> model = node->GetObject<MobilityModel>();
  const Vector v = Vector(10000.0, 0.0, 0.0);
  model->SetPosition(v);
}

void moveBack()
{
  Ptr<Node> node = NodeList::GetNode(4);
  Ptr<MobilityModel> model = node->GetObject<MobilityModel>();
  const Vector v = Vector(250.0, 0.0, 0.0);
  model->SetPosition(v);
}

int main(int argc, char *argv[])
{
  bool udp = true;
  bool useRts = false;
  double simulationTime = 15; //seconds

  CommandLine cmd(__FILE__);
  cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
  cmd.AddValue("udp", "UDP if set to 1, TCP otherwise", udp);
  cmd.AddValue("useRts", "Enable/disable RTS/CTS", useRts);
  cmd.Parse(argc, argv);

  if (useRts)
  {
    Config::SetDefault("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue("0"));
  }

  int channelWidth = 20;
  int sgi = 0;
  uint32_t payloadSize; //1500 byte IP packet
  if (udp)
  {
    payloadSize = 1472; //bytes
  }
  else
  {
    payloadSize = 1448; //bytes
    Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));
  }

  NodeContainer wifiNodes;
  wifiNodes.Create(5);

  YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
  YansWifiPhyHelper phy;
  phy.SetChannel(channel.Create());
  phy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

  WifiHelper wifi;
  wifi.SetStandard(WIFI_STANDARD_80211ac);
  WifiMacHelper mac;

  wifi.SetRemoteStationManager("ns3::MinstrelHtWifiManager");
  mac.SetType("ns3::AdhocWifiMac");

  NetDeviceContainer devices;
  devices = wifi.Install(phy, mac, wifiNodes);
  phy.EnablePcap("ADHOC", devices);

  // Set channel width
  // Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue(channelWidth));

  phy.Set ("ChannelWidth", UintegerValue (20));
  phy.Set ("Frequency", UintegerValue (5180));


  // Set guard interval
  Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/ShortGuardIntervalSupported", BooleanValue(sgi));

  for (uint32_t i = 0; i < NodeList::GetNNodes(); ++i)
  {
    Ptr<NetDevice> nd = devices.Get(i);
    Ptr<WifiNetDevice> wnd = nd->GetObject<WifiNetDevice>();
    Ptr<WifiPhy> wp = wnd->GetPhy();
    wp->SetNumberOfAntennas(2);
    wp->SetMaxSupportedTxSpatialStreams(2);
    wp->SetMaxSupportedRxSpatialStreams(2);
  }

  // mobility.
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

  positionAlloc->Add(Vector(0.0, 0.0, 0.0));
  positionAlloc->Add(Vector(50.0, 0.0, 0.0));
  positionAlloc->Add(Vector(100.0, 0.0, 0.0));
  positionAlloc->Add(Vector(150.0, 0.0, 0.0));
  positionAlloc->Add(Vector(200.0, 0.0, 0.0));
  positionAlloc->Add(Vector(250.0, 0.0, 0.0));
  mobility.SetPositionAllocator(positionAlloc);

  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

  mobility.Install(wifiNodes);

  /* Internet stack*/
  InternetStackHelper stack;
  stack.Install(wifiNodes);

  Ipv4AddressHelper address;
  address.SetBase("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces;
  interfaces = address.Assign(devices);
  Ipv4StaticRoutingHelper staticRoutingHelper;

  /* Setting routes */
  for (uint32_t i = 0; i < NodeList::GetNNodes(); ++i)
  {
    Ptr<Ipv4> ipv4 = wifiNodes.Get(i)->GetObject<Ipv4>();
    Ptr<Ipv4StaticRouting> staticRouting =
        staticRoutingHelper.GetStaticRouting(ipv4);

    if (i > 0)
    {
      staticRouting->AddHostRouteTo(
          Ipv4Address(("192.168.1." + std::to_string(i)).c_str()), 1);

      for (uint32_t j = 0; j < i - 1; ++j)
      {
        staticRouting->AddHostRouteTo(
            Ipv4Address(("192.168.1." + std::to_string(j + 1)).c_str()),
            Ipv4Address(("192.168.1." + std::to_string(i)).c_str()), 1);
      }
    }

    for (uint32_t j = i + 2; j < NodeList::GetNNodes(); ++j)
    {
      staticRouting->AddHostRouteTo(
          Ipv4Address(("192.168.1." + std::to_string(j + 1)).c_str()),
          Ipv4Address(("192.168.1." + std::to_string(i + 2)).c_str()), 1);
    }

    if (i < NodeList::GetNNodes() - 1)
    {
      staticRouting->AddHostRouteTo(
          Ipv4Address(("192.168.1." + std::to_string(i + 2)).c_str()), 1);
    }
  }

  /* Setting applications */
  ApplicationContainer serverApp;

  /* Generate traffic to populate the ARP tables before the start of the
   * applications */
  for (uint32_t i = 0; i < NodeList::GetNNodes(); ++i)
  {
    uint16_t port = 1337;
    PacketSinkHelper sink(
        "ns3::UdpSocketFactory",
        InetSocketAddress(interfaces.GetAddress(i), port));
    ApplicationContainer serverApp = sink.Install(wifiNodes.Get(i));
    serverApp.Start(Seconds(0.0));
    serverApp.Stop(Seconds(1));

    /* Node Before */
    if (i > 0)
    {
      UdpClientHelper client(
          InetSocketAddress(interfaces.GetAddress(i - 1), port));
      client.SetAttribute("MaxPackets", UintegerValue(10));
      client.SetAttribute("Interval", TimeValue(Seconds(.05)));
      client.SetAttribute("PacketSize", UintegerValue(12));

      ApplicationContainer clientApp = client.Install(wifiNodes.Get(i));
      clientApp.Start(Seconds(i * 0.02));
      clientApp.Stop(Seconds(1));
    }
    /* Node After */
    if (i < NodeList::GetNNodes() - 1)
    {
      UdpClientHelper client(
          InetSocketAddress(interfaces.GetAddress(i + 1), port));
      client.SetAttribute("MaxPackets", UintegerValue(10));
      client.SetAttribute("Interval", TimeValue(Seconds(.05)));
      client.SetAttribute("PacketSize", UintegerValue(12));

      ApplicationContainer clientApp = client.Install(wifiNodes.Get(i));
      clientApp.Start(Seconds(i * 0.02));
      clientApp.Stop(Seconds(1));
    }
  }

  /* Broadcast 10 times per second with the neighbours */
  for (uint32_t i = 0; i < NodeList::GetNNodes(); ++i)
  {
    uint16_t port = 9;
    PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(interfaces.GetAddress(i), port));
    ApplicationContainer serverApp = sink.Install(wifiNodes.Get(i));
    serverApp.Start(Seconds(0.0 + i * 0.01));
    serverApp.Stop(Seconds(simulationTime + 1));

    UdpClientHelper client(InetSocketAddress(interfaces.GetAddress(0).GetSubnetDirectedBroadcast(Ipv4Mask("/24")), port));
    client.SetAttribute("MaxPackets", UintegerValue(2 * simulationTime * 1 / (0.10)));
    client.SetAttribute("Interval", TimeValue(Seconds(.10)));
    client.SetAttribute("PacketSize", UintegerValue(12));

    ApplicationContainer clientApp = client.Install(wifiNodes.Get(i));
    clientApp.Start(Seconds(1.01 + i * 0.01));
    clientApp.Stop(Seconds(simulationTime + 1));
  }

  uint16_t port = 1234;
  std::string socketFactory;

  if (udp)
  {
    socketFactory = "ns3::UdpSocketFactory";
  }
  else
  {
    socketFactory = "ns3::TcpSocketFactory";
  }

  PacketSinkHelper sink(socketFactory, InetSocketAddress(interfaces.GetAddress(0), port));
  serverApp = sink.Install(wifiNodes.Get(0));
  serverApp.Start(Seconds(0.0));
  serverApp.Stop(Seconds(simulationTime + 1));

  OnOffHelper onoff(socketFactory, InetSocketAddress(interfaces.GetAddress(0), port));
  onoff.SetConstantRate(DataRate("50Mbps"), 1024);
  onoff.SetAttribute("StartTime", TimeValue(Seconds(1.000000)));
  ApplicationContainer clientApp = onoff.Install(wifiNodes.Get(NodeList::GetNNodes() - 1));
  clientApp.Start(Seconds(1.0));
  clientApp.Stop(Seconds(simulationTime + 1));
  Simulator::Stop(Seconds(simulationTime + 1));

  Simulator::Schedule(Seconds(5.0), move);
  Simulator::Schedule(Seconds(7.0), moveBack);

  Simulator::Run();
  Simulator::Destroy();
  return 0;
}
